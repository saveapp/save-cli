use std::collections::HashMap;

use crate::util;
use crate::Config;
use crate::Link;
use crate::SaveError;

pub fn get_all_links(config: &Config) -> Result<Vec<Link>, SaveError> {
    let endpoint: &str = "api/links";

    let url = util::url(&config, endpoint);
    let username = &config.username();
    let password = match config.password() {
        Some(pass) => pass,
        None => return Err(SaveError::ConfigMissing(String::from("password"))),
    };

    let client = reqwest::Client::new();
    let mut response = client
        .get(&url)
        .basic_auth(username, Some(password))
        .send()?;

    Ok(response.json()?)
}

pub fn get_link(id: u64, config: &Config) -> Result<Link, SaveError> {
    let endpoint: String = format!("api/links/{}", id);

    let api_url = util::url(&config, &endpoint);
    let username = &config.username();
    let password = match &config.password() {
        Some(pass) => pass,
        None => return Err(SaveError::ConfigMissing(String::from("password"))),
    };

    let client = reqwest::Client::new();
    let response = client
        .get(&api_url)
        .basic_auth(username, Some(password))
        .send()?;
    match response.error_for_status() {
        Ok(mut r) => Ok(r.json()?),
        Err(e) => Err(SaveError::ApiError(format!("{}", e))),
    }
}

pub fn remove_link(id: u64, config: &Config) -> Result<reqwest::Response, SaveError> {
    let endpoint: String = format!("api/links/{}", id);

    let api_url = util::url(&config, &endpoint);
    let username = &config.username();
    let password = match config.password() {
        Some(pass) => pass,
        None => return Err(SaveError::ConfigMissing(String::from("password"))),
    };

    let client = reqwest::Client::new();
    Ok(client
        .delete(&api_url)
        .basic_auth(username, Some(password))
        .send()?)
}

pub fn save_link(
    url: &str,
    annotation: Option<&str>,
    config: &Config,
) -> Result<reqwest::Response, SaveError> {
    const ENDPOINT: &str = "api/links";
    let api_url = util::url(&config, ENDPOINT);

    let username = &config.username();
    let password = match config.password() {
        Some(pass) => pass,
        None => return Err(SaveError::ConfigMissing(String::from("password"))),
    };

    let mut link_map: HashMap<&str, &str> = HashMap::new();
    link_map.insert("url", url);
    if annotation.is_some() {
        link_map.insert("annotation", annotation.unwrap());
    }

    let client = reqwest::Client::new();
    Ok(client
        .post(&api_url)
        .basic_auth(username, Some(password))
        .json(&link_map)
        .send()?)
}
