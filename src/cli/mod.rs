use pager::Pager;

mod list_cmd;
mod open_cmd;
mod remove_cmd;
mod save_cmd;

use save::Link;

pub fn list_links(filter: Option<&str>, print: bool) {
    let links: Vec<Link> = match list_cmd::list(filter) {
        Ok(l) => l,
        Err(e) => {
            eprintln!("Error listing links: {}", e);
            return;
        }
    };
    if !print {
        Pager::new().setup();
    }
    for link in links {
        println!("{:#}", link);
    }
}

pub fn open_link(ids: Vec<u64>) {
    if !ids.is_empty() {
        let opened = open_cmd::open(ids);
        match opened {
            Ok(count) => println!(
                "Opened {} {}",
                count,
                match count {
                    1 => "link",
                    _ => "links",
                }
            ),
            Err(e) => eprintln!("Error opening links: {}", e),
        }
    }
}

pub fn remove_link(id: u64) {
    match remove_cmd::remove(id) {
        Ok(mut api_response) => {
            let _response_content = match api_response.text() {
                Ok(content) => content,
                Err(_) => {
                    eprintln!("Error reading response content");
                    return;
                }
            };

            match api_response.error_for_status() {
                // TODO: print info about saved link (id?)
                Ok(_) => println!("Link removed"),
                Err(e) => {
                    eprintln!("Error removing link: {}", e);
                }
            }
        }
        Err(e) => eprintln!("Error removing link: {}", e),
    }
}

pub fn save_link(url: &str, annotation: Option<&str>) {
    match save_cmd::save(url, annotation) {
        Ok(mut api_response) => {
            // TODO: print info about saved link (id?)
            let _response_content = match api_response.text() {
                Ok(content) => content,
                Err(_) => {
                    eprintln!("Error reading response content");
                    return;
                }
            };
            match api_response.error_for_status() {
                // TODO: use success message from API response?
                Ok(_) => println!("Link saved"),
                Err(e) => {
                    eprintln!("Error saving link: {}", e);
                }
            }
        }
        Err(e) => eprintln!("{}", e),
    }
}
