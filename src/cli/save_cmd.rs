use save::api;
use save::Config;
use save::SaveError;

pub fn save(_url: &str, annotation: Option<&str>) -> Result<reqwest::Response, SaveError> {
    let config = Config::from_file(None)?;
    Ok(api::save_link(_url, annotation, &config)?)
}
